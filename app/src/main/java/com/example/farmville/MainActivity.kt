package com.example.farmville

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.farmville.databinding.ActivityMainBinding
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    val metalChannel = Channel<Int>(3)
    val crystalChannel = Channel<Int>(3)
    val deuteriumChannel = Channel<Int>(3)

    var metalDirtTotal = 0
    var crystalDirtTotal = 0
    var deuteriumDirtTotal = 0

    var metalStacksTotal = 0
    var crystalStacksTotal = 0
    var deuteriumStacksTotal = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        manageMetal()
        manageCrystal()
        manageDeuterium()

        binding.metalDirt.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                // Vaig haver de posar la seguent linea per "comprovar que el channel
                // te algun valor abans de rebre un altre", ja que si trec aixo i spamejo
                // a clicks, el recompte de metalDirt se'n va a negatius i el recompte
                // de metalStack = al nombre de clicks qe hagis fet i no té aquest "cooldown" de 1s
                // Tot i aixi, segueix existint el bug que si deixes passar el temps i esta
                // a 3 i clikes no baixa a 2, sino qe has de clickar dos cops
                if(!metalChannel.isEmpty){
                    println(metalDirtTotal)
                    metalDirtTotal--
                    println(metalDirtTotal)
                    metalStacksTotal++
                    metalChannel.receive()
                    binding.metalDirtTotal.text = "$metalDirtTotal"
                    binding.metalStacksTotal.text = "$metalStacksTotal"
                }
            }
        }

        binding.crystalDirt.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                if(!crystalChannel.isEmpty){
                    crystalDirtTotal--
                    crystalStacksTotal++
                    crystalChannel.receive()
                    binding.crystalDirtTotal.text = "$crystalDirtTotal"
                    binding.crystalStacksTotal.text = "$crystalStacksTotal"
                }
            }
        }

        binding.deuteriumDirt.setOnClickListener {
            CoroutineScope(Dispatchers.Main).launch {
                if(!deuteriumChannel.isEmpty){
                    deuteriumDirtTotal--
                    deuteriumStacksTotal++
                    deuteriumChannel.receive()
                    binding.deuteriumDirtTotal.text = "$deuteriumDirtTotal"
                    binding.deuteriumStacksTotal.text = "$deuteriumStacksTotal"
                }
            }
        }
    }

    private fun manageMetal() {
        CoroutineScope(Dispatchers.Main).launch {
            while(true){
                delay(1000)
                metalDirtTotal++
                metalChannel.send(metalDirtTotal)
                binding.metalDirtTotal.text = "$metalDirtTotal"
            }
        }
    }

    private fun manageCrystal() {
        CoroutineScope(Dispatchers.Main).launch {
            while(true){
                delay(2000)
                crystalDirtTotal++
                crystalChannel.send(crystalDirtTotal)
                binding.crystalDirtTotal.text = "$crystalDirtTotal"
            }
        }
    }

    private fun manageDeuterium() {
        CoroutineScope(Dispatchers.Main).launch {
            while(true){
                delay(3000)
                deuteriumDirtTotal++
                deuteriumChannel.send(deuteriumDirtTotal)
                binding.deuteriumDirtTotal.text = "$deuteriumDirtTotal"
            }
        }
    }
}
